using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBallController : MonoBehaviour
{
    [SerializeField] private float DestroyGranceTime = 1.5f; // Destroyまでの猶予時間
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame

    void Update()
    {
        // 接地するより低い高度になった場合
        if(transform.position.y <= 0.5)
        {
            // 一定時間後にDestroyする。
            StartCoroutine(DestroyCannonball());
        }
    }
    /// <summary>
    /// 一定時間後にオブジェクトをDestroyする
    /// </summary>
    /// <returns></returns>
    IEnumerator DestroyCannonball()
    {
        yield return new WaitForSeconds(DestroyGranceTime);
        Destroy(this.gameObject);
    }
}
