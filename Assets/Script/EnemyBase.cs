using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBase : MonoBehaviour
{
    protected int Point { get; set;}

    protected EnemyBase(int i)
    {
        Point = i;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
        GameManager.Score += Point;
    }
}
