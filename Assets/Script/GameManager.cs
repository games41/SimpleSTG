using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public Text TextFrame;

    // static propertyってありなのか？
    public static int Score { get; set;}
    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    void Initialize()
    {
        // メインシーンに遷移するときにスコアを初期化する
        if(SceneManager.GetActiveScene().name == "MainScene")
        {
            Score = 0;
        }
    }

    // Update is called once per framesss
    void Update()
    {
        TextFrame.text = $"Score:{GameManager.Score}";
    }
}
