using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeManager : MonoBehaviour
{
    // UI Text指定用
    public Text TextFrame;
    // 表示用変数
    [SerializeField] private int FinishTime;
    // やるならプロパティだろうな
    // MonoBehaviourじゃなくて良さそうだなー
    // 公開の感じとかも最悪そう
    // Start is called before the first frame update
    void Start()
    {
        StartCountDown();
    }
    void Update()
    {
        TextFrame.text = $"Time:{FinishTime}";
        
        // 時間が0になったら、結果画面に遷移する
        if(FinishTime <= 0)
        {
            ChangeScene();
        }

    }
    /// <summary>
    /// カウントダウンの開始
    /// コルーチンでタイマー実装するのいまいちそうだなー
    /// </summary>
    public void StartCountDown()
    {
        StartCoroutine(CountDownCoroutine());
    }

    /// <summary>
    /// 結果画面に遷移する
    /// </summary>
    private void ChangeScene()
    {
        SceneManager.LoadScene("ResultScene");
    }


    /// <summary>
    /// カウントダウン用のコルーチン
    /// </summary>
    /// <returns></returns>
    IEnumerator CountDownCoroutine()
    {
        while (FinishTime > 0)
        {
            yield return new WaitForSecondsRealtime(1);
            FinishTime--;
        }
    }


}
