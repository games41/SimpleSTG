using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private BoxCollider collidar;
    // private GameManager gameManager;
    private int addPoint = 10;
    // Start is called before the first frame update
    void Start()
    {
        collidar = GetComponent<BoxCollider>();
        //gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        // スコア加算用
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {

        Destroy(this.gameObject);
        GameManager.Score += addPoint;
    }
}
