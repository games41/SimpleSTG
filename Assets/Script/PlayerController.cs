using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private double SideSensi = 1f;
    [SerializeField] private double UpSensi = 1f;
    [SerializeField] private GameObject ProjectileObj;
    private GameObject Cylinder;
    private GameObject RotatePoint;
    private double AccAngleX;
    const float DefaultFirePower = 20f;
    const float PowerChargeInterval = 1.5f; // Seconds
    [SerializeField] float PowerMultiplier = 0.5f;
    private Coroutine RetCoroutine;
    private Behaviour HeloBehav;

    // アクセサメソッド
    public GameObject Projectile { get; set; }
    private 
    // Start is called before the first frame update
    void Start()
    {
        RotatePoint = transform.Find("RotatePoint").gameObject;
        Cylinder = RotatePoint.transform.Find("Cylinder").gameObject;
        // 大砲の向き制限用の角度
        AccAngleX = RotatePoint.transform.rotation.x;
        // チャージ中のエフェクト
        HeloBehav = (Behaviour)this.GetComponent("Halo");
    }

    // Update is called once per frame
    void Update()
    {
        // 大砲の回転処理
        this.rotateCanon();

        // たまのチャージ開始
        if (Input.GetButtonDown("Fire1"))
        {
            RetCoroutine = StartCoroutine(ChargePowerLoop());
        }

        // たまの発射
        if (Input.GetButtonUp("Fire1"))
        {
            this.fire();
            StopCoroutine(RetCoroutine);
            PowerMultiplier = 0.5f;
        }

        // チャージに応じたエフェクトの追加
        this.applyChargeEffect();

    }

    private void rotateCanon()
    {
        var vert = Input.GetAxis("Vertical");
        var hori = Input.GetAxis("Horizontal");
        float clamedAngleX = 0;

        // 大砲の垂直方向回転
        if (RotatePoint != null)
        {
            double x = -UpSensi * Input.GetAxis("Vertical") * Time.deltaTime;

            // 垂直方向回転の角度の制限
            // 10から80まで
            AccAngleX += x;
            clamedAngleX = Mathf.Clamp((float)AccAngleX, 10, 80);

            var adjustAngle = AccAngleX - clamedAngleX;
            x = x - adjustAngle;
            AccAngleX = clamedAngleX;

            RotatePoint.transform.Rotate((float)x, 0, 0);
        }

        // 大砲の水平方向回転
        double y = SideSensi * System.Convert.ToDouble(Input.GetAxis("Horizontal")) * System.Convert.ToDouble((Time.deltaTime));
        Debug.Log("y:" + y + " time:" + Time.deltaTime + " Axis" + Input.GetAxis("Horizontal"));
        this.transform.Rotate(0, (float)y, 0);

    }
    /// <summary>
    /// 砲弾の射出処理
    /// 砲弾を生成し、大砲の角度に応じて射出する
    /// </summary>
    private void fire()
    {
        Transform mazzlePoint;
        Rigidbody rb = null;
        mazzlePoint = Cylinder.transform.Find("MazzlePoint");

        rb = Instantiate(ProjectileObj, mazzlePoint.position, Quaternion.identity).GetComponent<Rigidbody>();
        rb.AddForce(Cylinder.transform.up * DefaultFirePower * PowerMultiplier, ForceMode.Impulse);
    }

    /// <summary>
    /// チャージショットのpower増加
    /// </summary>
    /// <returns></returns>
    private IEnumerator ChargePowerLoop()
    {
        while (true)
        {

            if (PowerMultiplier >= 1.5f)
            {
                yield break;
            }

            PowerMultiplier += 0.5f;
            yield return new WaitForSeconds(PowerChargeInterval);
        }
    }

    /// <summary>
    /// チャージショットの乗算係数の値に応じてエフェクトを追加する
    /// </summary>
    private void applyChargeEffect()
    {
        if (PowerMultiplier == 1.5f)
        {
            HeloBehav.enabled = true;
        }
        else
        {
            HeloBehav.enabled = false;
        }
    }
}

