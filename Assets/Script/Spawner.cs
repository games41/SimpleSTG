using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject enemyPrefab;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnLoop());
    }

    /// <summary>
    /// �G�o����Coroutine
    /// </summary>
    /// <returns></returns>
    private IEnumerator SpawnLoop()
    {
        while (true)
        {
            var randFarX = Random.Range(-20f, 20f);
            var randFarY = Random.Range(3f, 20f);

            var randNearX = Random.Range(-20f, 20f);
            var randNearY = Random.Range(3f, 15f);

            var spawnPositionFar = new Vector3(randFarX, randFarY, 20);
            var spawnPositionNear = new Vector3(randNearX, randNearY, 0);

            Instantiate(enemyPrefab, spawnPositionFar, Quaternion.identity);
            Instantiate(enemyPrefab, spawnPositionNear, Quaternion.identity);

            yield return new WaitForSeconds(10);
        }
    }
}
